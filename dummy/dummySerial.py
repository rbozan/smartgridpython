#!/bin/env python2
import os
import pty
import serial
from ledstrip import Table
import sys
import time
sys.path.append("../")
from mysensors_gateway_connector import MySenTypes
import binascii
import struct
import re


def chunks(s, n):
    """Produce `n`-character chunks from `s`."""
    for start in range(0, len(s), n):
        yield s[start:start + n]


class DummySerial():
    """
    Class to fake a serial port.
    """
    master, fd = pty.openpty()
    name = os.ttyname(fd)
    pty.tty.B115200
    ser = serial.Serial(name, 115200, rtscts=True, dsrdtr=True)
    tables = [Table()]

    def __init__(self):
        print(self.name)

        # Start processing serial if Python program is not an unit test
        if sys.argv[0].find('unittest') == -1:
            self.process()

    def get_message(self):
        """
        Function to retrieve a new incoming message.
        """
        return os.read(self.master, 1024)

    def process(self):
        """
        Parse incoming messages.
        Whenever the message is parsed, this function gets recalled so it repeats itself.
        """
        msg = self.get_message()
        msg = msg.split(';')
        if (len(msg) < 5):
            return self.process()

        destination = int(msg[0])
        command = int(msg[2])
        type = int(msg[4])
        child_id = int(msg[1])
        payload = msg[5]

        if type == MySenTypes.V_VAR1:
            """
            Reset table sections
            """
            print("Reset table section message received")
            self.create_table(1)
            self.create_table(2)
            self.create_table(3)
            self.create_table(4)

            print('Created 4 tables')

            " table id, position, module id "
            self.place_module(1, 0, 2090432304)
            self.place_module(1, 1, 444756283)
            self.place_module(1, 2, 1947443573)
            self.place_module(1, 5, 1947443573)

            self.place_module(1, 3, 1945699104)

            print('Created modules for table section 1')

            self.place_module(2, 0, 1947440355)
            self.place_module(2, 3, 2090795360)

            print('Created modules for table section 2')

            self.place_module(3, 0, 2091095888)

            print('Created modules for table section 3')

            self.connect_tables(2, 4)

        elif type == MySenTypes.V_VAR4:
            """
            New flow configuration
            """
            print("Received new flow configuration message")
            # Delete last element as it's a newline character
            # Parse chunks
            payload_chunks = list(chunks(payload, 2))[:-1]
            payload_chunks = map(lambda x: bin(int(x, 16)), payload_chunks)

            # Network-header
            voltage = int(payload_chunks[0][2:4], 2)

            # print("Voltage is " + str(voltage))

            # Flow-byte
            for chunk in payload_chunks:
                speed, reversed, load, state = map(lambda x: int(x, 2), re.findall(
                    r'(\d\d\d)(\d)(\d\d)(\d\d)', chunk[2:].zfill(8))[0])
                reversed = bool(reversed)
                # print("Flow byte information: " + speed +

        elif type == MySenTypes.V_RGB:
            self.tables[0].ledstrip_set_color(child_id, payload)
            print("Change flow color message received")

        elif type == MySenTypes.V_VAR5:
            print("Time since startup message received")

        else:
            print("Received unhandled message")

        self.process()

    def write_to_master(self, table_section_id, payload, command, type, child_id=0):
        """
        Function to send a message to the application.
        Based of send_serial_message function.
        More information about what each command does is found in the documentation.
        """
        message = '{0};{1};{2};0;{3};{4}\n'.format(
            table_section_id, child_id, command, type, payload)

        os.write(self.master, message)

    def place_module(self, table_id, position, module_id):
        """
        Send a message to place a module on a table section.
        """
        return self.write_to_master(table_id, module_id, 1, MySenTypes.V_VAR2, position)

    def create_table(self, table_id):
        """
        Send a message to create a table in the application.
        """
        return self.write_to_master(table_id, 0, 0, MySenTypes.S_CUSTOM)

    def connect_tables(self, table1, table2):
        """
        Send a message to connect two tables with each other.
        The second argument is the RFID-tag identification.
        The last argument is the side (north / south).
        """
        self.write_to_master(table1, 2090854272, 1, MySenTypes.V_VAR3, 0)
        self.write_to_master(table2, 2597787091, 1, MySenTypes.V_VAR3, 1)
        print("Connect table " + str(table1) + " with table " + str(table2))


if __name__ == '__main__':
    DummySerial()
