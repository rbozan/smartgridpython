VOLTAGE_LOW = 0
VOLTAGE_MEDIUM = 1
VOLTAGE_HIGH = 2


class Table():
    voltage_colors = [None] * 6

    def ledstrip_set_color(self, id, rgb):
        self.voltage_colors[id] = rgb
