# REST endpoints

All requests should append a prefix of `/api`. Meaning `/tablesections` would instead be `/api/tablesections`.

## Get all table sections

```
url:	/tablesections/
method:	GET
```

Example response:

```
{
	"table_sections": [
		{
			"id": 1,
			"pos": [
				3, 5
			],
			"type": 1
		},
		{
			"id": 2,
			"pos": [
				5, 3
			],
			"type": 2
		}
	]
}
```

## Get all modules of a given table section

```
url:	/tablesections/:id/modules/
method: GET
```

Example response:

```
{
	"modules": [
		{
			"id": 2090677472,
			"locationId": 0
		},
		{
			"id": 1945556720,
			"locationId": 1
		}
	]
}
```

## Get all modules

```
url:	/modules/
method:	GET
```

Example response:

```
{
	"modules": [
		{
			"id": 3920381746
		},
		{
			"id": 2310312452
		}
	]
}
```

## Get info of a given module

```
url:	/modules/:id/
method:	GET
```

Example response:

```
{
	"id": 3920381746,
	"name": "Nuclear Power Plant 2",
	"power": -500,
	"voltage": "High"
}
```

## Get configurations of a given module

```
url:	/modules/:id/configs/
method:	GET
```

Example response:

```
{
	"configurations": [
		{
			"id": 9,
			"max": 1200,
			"min": 0,
			"name": "Generator",
			"role": "production",
			"value": 500
		}
	]
}
```

## Get given configuration of a given module

```
url:	/modules/:id/configs/:config_id/
method:	GET
```

Example response:

```
{
	"id": 9,
	"max": 1200,
	"min": 0,
	"name": "Generator",
	"role": "production",
	"value": 500
}
```

## Set property of given configuration on a given module

```
url:	/modules/:id/configs/:config_id/
method:	PUT
```

Example request content:

```
{
	"name": "Old Generator"
}
```

Example response:

```
{
	"id": 9,
	"max": 1200,
	"min": 0,
	"name": "Old Generator",
	"role": "production",
	"value": 500
}
```

## Set a flow colour

The color is equivalent to certain voltages and loads.

| ID | Description    |
| -- | -------------- |
| 0  | low voltage    |
| 1  | medium voltage |
| 2  | high voltage   |
| 3  | normal load    |
| 4  | high load      |
| 5  | stressed load  |

```
url:	/flowcolor/:id/
method:	PUT
```

Example request content:

```
{
	"value": "ff0000"
}
```

## Get powerboundaries for voltages and flows

```
url:	/powerboundaries/
method:	GET
```

Example response:

```
{
	"boundaries": {
		"0": {
			"high": 0.75,
			"critical": 300
		},
		"1": {
			"high": 0.75,
			"critical": 500
		},
		"2": {
			"high": 0.75,
			"critical": 1300
		}
	}
}
```

* 0/1/2: voltages. 0 means low, 1 means medium, 2 means high
* High: modifier for high load
* Critical: absolute value for critical load

## Set one of the boundaries for load

```
url:	/powerboundaries/
method:	PUT
```

Example request content:

```
{
	"voltage": 0,
	"load": 2,
	"value": 500
}
```

## Get the grid

```
url:	/grid/
method:	GET
```

Example response:

```
{
	"flow_segments": [
		{
			"start_pos": (1,2)
			"end_pos": (1,2)
			"direction": 0
			"enabled": true
		}
	],
	"modules": [

	]
}
```

Where `direction` is either 0 (forwards) or 1 (backwards).

## Flowsegment

```
url:	/flowsegment/:id/
method:	PUT
```

Where id comes from `/grid/` in the "flow_segments" array (based on order of the result).

Exampe request content:

```
{
	"enabled": "true"
}
```

## Reboot all table sections

```
url:	/reboot/
method:	PUT
```
