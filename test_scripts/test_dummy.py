import sys
sys.path.append("./dummy")
from dummySerial import DummySerial


def test_valid_serial():
    assert DummySerial.name[:8] == '/dev/pts'
