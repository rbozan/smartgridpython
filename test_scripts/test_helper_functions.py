import pytest
import sys
sys.path.append("../")
from settings import Voltages, Load, GET_LOAD, Speed, GET_SPEED


def test_GET_LOAD():
    assert GET_LOAD(Voltages.LOW, 621) == Load.CRITICAL
    assert GET_LOAD(Voltages.MEDIUM, 489) == Load.HIGH
    assert GET_LOAD(Voltages.HIGH, 2) == Load.NORMAL


def test_GET_SPEED():
    assert GET_SPEED(50) == Speed.NORMAL
    assert GET_SPEED(200) == Speed.FAST
    assert GET_SPEED(300) == Speed.FASTER
    assert GET_SPEED(400) == Speed.FASTEST
